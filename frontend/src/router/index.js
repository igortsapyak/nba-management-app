import Vue from 'vue';
import Router from 'vue-router';
import Main from '@/components/Main';
import Team from '@/components/Team';
import AddPlayer from '@/components/AddPlayer';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main
    },
    {
      path: '/teams/:id',
      name: 'Team',
      component: Team
    },
    {
      path: '/teams/:id/players/add',
      component: AddPlayer
    },

    {
      path: '**',
      redirect: '/'
    }
  ]
})
