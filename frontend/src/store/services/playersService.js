import { store } from '../store'
import {SET_PLAYERS} from "../action-name";
import {HttpClient} from "@/common/http";

export const PlayersService = {

  getPlayersByTeam(teamId) {
    return HttpClient.get(`/players/team/${teamId}`).then(response => {
      store.commit(SET_PLAYERS, {playersForTeam: response.data, teamId: teamId});
    })
  },

  createPlayerForTeam(player, teamId) {
    return HttpClient.post(`/players/team/${teamId}`, player)
  }

};
