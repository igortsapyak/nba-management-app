import {store} from '../store'
import {SET_TEAMS} from "../action-name";
import {HttpClient} from "@/common/http";

export const TeamsService = {

  getTeams() {
    return HttpClient.get('/teams').then(response => {
      store.commit(SET_TEAMS, response.data);
    })
  }

};
