import Vue from 'vue';
import Vuex from 'vuex'
import {playersForTeam} from "./modules/players";
import {teams} from "./modules/teams";

Vue.use(Vuex);

export const store = new Vuex.Store({
  modules: {teams, playersForTeam}
});
