import Vue from 'vue';
import Vuex from 'vuex';
import {SET_PLAYERS} from "../action-name";

Vue.use(Vuex);

export const playersForTeam = {
  state: {
    playersForTeam: {
      players: [],
      teamId: null
    }
  },
  getters: {
    playersForTeam: state => state.playersForTeam
  },
  mutations: {
    [SET_PLAYERS](state, data) {
      state.playersForTeam.players = data.playersForTeam;
      state.playersForTeam.teamId = data.teamId;
    }
  }
};
