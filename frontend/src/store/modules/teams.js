import {SET_PLAYERS, SET_TEAMS} from "../action-name";

export const teams = {
  state: {
    teams: []
  },
  getters: {
    teams: state => state.teams
  },
  mutations: {
    [SET_TEAMS](state, teams) {
      state.teams = teams;
    }
  }
};
