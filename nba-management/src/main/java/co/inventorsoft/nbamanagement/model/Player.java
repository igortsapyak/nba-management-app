package co.inventorsoft.nbamanagement.model;


import co.inventorsoft.nbamanagement.common.AbstractIdentifiable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Getter
@Setter
@Entity
@Table(name = "player")
public class Player extends AbstractIdentifiable {

    private String fullName;
    private String phone;
    private Double height;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "team_id")
    private Team team;

}
