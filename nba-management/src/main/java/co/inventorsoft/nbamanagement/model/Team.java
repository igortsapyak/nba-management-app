package co.inventorsoft.nbamanagement.model;

import co.inventorsoft.nbamanagement.common.AbstractIdentifiable;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "team")
public class Team extends AbstractIdentifiable {

    @JsonProperty("team_id")
    private String teamId;
    private String abbreviation;
    private Boolean active;
    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    private String lastName;
    private String conference;
    private String division;
    @JsonProperty("site_name")
    private String siteName;
    private String city;
    private String state;
    @JsonProperty("full_name")
    private String fullName;

}
