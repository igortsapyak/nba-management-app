package co.inventorsoft.nbamanagement.model.view;

import co.inventorsoft.nbamanagement.common.AbstractIdentifiable;
import co.inventorsoft.nbamanagement.model.Player;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static co.inventorsoft.nbamanagement.common.ResponseParametersConstants.FULL_NAME;
import static co.inventorsoft.nbamanagement.common.ResponseParametersConstants.HEIGHT;
import static co.inventorsoft.nbamanagement.common.ResponseParametersConstants.PHONE;


@Getter
@Setter
public class PlayerView extends AbstractIdentifiable {

    private String fullName;
    private String phone;
    private Double height;

    public PlayerView(final Player player) {
        this.id = player.getId();
        this.fullName = player.getFullName();
        this.phone = player.getPhone();
        this.height = player.getHeight();
    }

    public PlayerView(Object player) {
        Map<String, Object> playerObject = new HashMap<>((Map<? extends String, ?>) player);
        this.fullName = (String) playerObject.get(FULL_NAME);
        this.phone = PHONE;
        this.height = (Double) playerObject.get(HEIGHT);
    }

}
