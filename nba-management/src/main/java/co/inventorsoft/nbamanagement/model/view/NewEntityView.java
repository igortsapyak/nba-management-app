package co.inventorsoft.nbamanagement.model.view;

import co.inventorsoft.nbamanagement.common.AbstractIdentifiable;

/**
 * Defines transformation between value object/DTO and application's entity
 *
 * @param <T> entity type
 */
public interface NewEntityView<T extends AbstractIdentifiable> {

    T create();

}