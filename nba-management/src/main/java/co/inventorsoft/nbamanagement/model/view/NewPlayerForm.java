package co.inventorsoft.nbamanagement.model.view;

import co.inventorsoft.nbamanagement.model.Player;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


@Getter
@Setter
public class NewPlayerForm implements NewEntityView<Player> {

    @NotNull
    @NotEmpty
    private String fullName;

    @NotNull
    @NotEmpty
    private String phone;

    @NotNull
    private Double height;

    @Override
    public Player create() {
        Player player = new Player();
        player.setFullName(this.fullName);
        player.setPhone(this.phone);
        player.setHeight(this.height);
        return player;
    }
}
