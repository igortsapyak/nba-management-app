package co.inventorsoft.nbamanagement.repository;

import co.inventorsoft.nbamanagement.model.Team;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TeamRepository extends CrudRepository<Team, Long> {

    List<Team> findAll();

    @Query(value = "SELECT t.teamId from Team t where t.id = :id")
    String getStringTeamIdById(@Param("id") Long id);

}
