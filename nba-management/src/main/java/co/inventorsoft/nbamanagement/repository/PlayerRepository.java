package co.inventorsoft.nbamanagement.repository;

import co.inventorsoft.nbamanagement.model.Player;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PlayerRepository extends CrudRepository<Player, Long> {

    List<Player> getByTeamId(Long teamId);

}
