package co.inventorsoft.nbamanagement.common;

public class ResponseParametersConstants {

    public static final String FULL_NAME = "display_name";
    public static final String HEIGHT = "height_cm";
    public static final String PHONE = "(000) 000-00-00";
    public static final String PLAYER = "player";

}
