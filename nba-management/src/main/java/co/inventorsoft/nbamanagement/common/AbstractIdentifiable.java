package co.inventorsoft.nbamanagement.common;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Base class for all application entities.
 * The purpose of this class is to bring primary key field into
 * all application entities. This has a couple of benefits:
 * <ul>
 * <li>we don't need to write the same mapping for each entity;</li>
 * <li>we can switch between automatic id generation strategy for all entities if we need to;</li>
 * </ul>
 */
@Getter
@Setter
@MappedSuperclass
public abstract class AbstractIdentifiable {

    public static final String ID_FIELD = "id";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;
}
