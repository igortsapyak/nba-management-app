package co.inventorsoft.nbamanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@EntityScan
@SpringBootApplication
public class NbaManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(NbaManagementApplication.class, args);
    }
}
