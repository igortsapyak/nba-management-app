package co.inventorsoft.nbamanagement.api;

import co.inventorsoft.nbamanagement.model.view.NewPlayerForm;
import co.inventorsoft.nbamanagement.model.view.PlayerView;
import co.inventorsoft.nbamanagement.service.PlayerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

import static java.lang.String.format;

@RestController
@RequestMapping("/players")
@RequiredArgsConstructor
public class PlayerController {

    private final PlayerService playerService;

    @PostMapping("/team/{teamId}")
    public ResponseEntity create(@Valid @RequestBody NewPlayerForm newPlayerForm, @PathVariable Long teamId) {
        Long createdPlayerId = playerService.create(newPlayerForm, teamId);
        return ResponseEntity.created(URI.create(format("/players/%d", createdPlayerId))).build();
    }

    @GetMapping("/team/{teamId}")
    public ResponseEntity<List<PlayerView>> getByTeam(@PathVariable Long teamId) {
        return ResponseEntity.ok(playerService.getByTeam(teamId));
    }

}
