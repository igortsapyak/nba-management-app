package co.inventorsoft.nbamanagement.api;

import co.inventorsoft.nbamanagement.model.Team;
import co.inventorsoft.nbamanagement.service.TeamService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/teams")
@RequiredArgsConstructor
public class TeamController {

    private final TeamService teamService;

    @GetMapping
    public ResponseEntity<List<Team>> get() {
        return ResponseEntity.ok(teamService.getAll());
    }

}
