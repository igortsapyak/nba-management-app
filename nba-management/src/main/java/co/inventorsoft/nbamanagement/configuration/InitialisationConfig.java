package co.inventorsoft.nbamanagement.configuration;

import co.inventorsoft.nbamanagement.model.Team;
import co.inventorsoft.nbamanagement.service.TeamService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.List;

@RequiredArgsConstructor
@Configuration
@DependsOn("restTemplate")
public class InitialisationConfig {

    private final RestTemplate restTemplate;
    private final TeamService teamService;

    @Value("${erikberg.teams.url}")
    private String teamsUrl;

    @PostConstruct
    public void initialiseData() {
        ResponseEntity<List<Team>> exchangeResult = restTemplate.exchange(teamsUrl, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<Team>>() {
                });
        teamService.saveInitialData(exchangeResult.getBody());
    }

}
