package co.inventorsoft.nbamanagement.service;

import co.inventorsoft.nbamanagement.model.Player;
import co.inventorsoft.nbamanagement.model.view.NewPlayerForm;
import co.inventorsoft.nbamanagement.model.view.PlayerView;
import co.inventorsoft.nbamanagement.repository.PlayerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class PlayerService {

    private final PlayerRepository playerRepository;
    private final TeamService teamService;
    private final XmlStatsApiService xmlStatsApiService;

    public Long create(NewPlayerForm newPlayerForm, Long teamId) {
        Player player = newPlayerForm.create();
        player.setTeam(teamService.getTeam(teamId));
        Player savedPlayer = playerRepository.save(player);
        return savedPlayer.getId();
    }

    public List<PlayerView> getByTeam(final Long teamId) {
        List<PlayerView> players = new ArrayList<>();

        List<PlayerView> playersForTeamFromApi = xmlStatsApiService.getPlayersForTeam(teamService.getStringTeamIdById(teamId));
        List<PlayerView> playersForTeamLocal = playerRepository.getByTeamId(teamId).stream().map(PlayerView::new).collect(Collectors.toList());

        players.addAll(playersForTeamFromApi);
        players.addAll(playersForTeamLocal);

        return players;
    }
}
