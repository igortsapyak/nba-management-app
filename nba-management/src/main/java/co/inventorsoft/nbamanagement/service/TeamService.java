package co.inventorsoft.nbamanagement.service;

import co.inventorsoft.nbamanagement.model.Team;
import co.inventorsoft.nbamanagement.repository.TeamRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@RequiredArgsConstructor
@Service
public class TeamService {

    private final TeamRepository teamRepository;

    public Team getTeam(final Long teamId) {
        return teamRepository.findById(teamId).orElseThrow(NoSuchElementException::new);
    }

    public List<Team> getAll() {
        return teamRepository.findAll();
    }

    public String getStringTeamIdById(final Long teamId) {
        return teamRepository.getStringTeamIdById(teamId);
    }

    public void saveInitialData(List<Team> teams) {
        teamRepository.saveAll(teams);
    }
}
