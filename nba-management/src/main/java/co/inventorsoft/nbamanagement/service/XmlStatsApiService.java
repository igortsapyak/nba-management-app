package co.inventorsoft.nbamanagement.service;


import co.inventorsoft.nbamanagement.model.view.PlayerView;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static co.inventorsoft.nbamanagement.common.ResponseParametersConstants.PLAYER;

@RequiredArgsConstructor
@Service
public class XmlStatsApiService {

    @Value("${erikberg.teams.url}")
    private String teamsUrl;

    @Value("${erikberg.teams.players.url}")
    private String playersUrl;

    @Value("${erikberg.accessToken}")
    private String accessToken;

    private final RestTemplate restTemplate;

    public List<PlayerView> getPlayersForTeam(final String teamId) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(playersUrl)
                .queryParam("team_id", teamId);

        ResponseEntity<List<Map>> exchange = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, getStringHttpEntity(),
                new ParameterizedTypeReference<List<Map>>() {
                });

        return buildPlayersViewFromResponseMap(exchange.getBody());
    }

    private HttpEntity<String> getStringHttpEntity() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", accessToken);
        return new HttpEntity<>(httpHeaders);
    }


    public List<PlayerView> buildPlayersViewFromResponseMap(List<Map> response) {
        List<Map> collect = response.stream().map(map -> (Map) map.get(PLAYER)).collect(Collectors.toList());
        return collect.stream().map(PlayerView::new).collect(Collectors.toList());
    }

}
